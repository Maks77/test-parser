import { RequestManager } from "./request-manager";
import { Worker } from 'worker_threads';

export class Parser {

  public async parse(url: string): Promise<void> {
    try {
      const data = await RequestManager.getResponse(url)
      const jsonData = JSON.parse(data)

      const elements = jsonData?.data?.children

      if (!elements || elements.length === 0) {
        throw new Error('Elements not found')
      }

      await Promise.all(jsonData.data.children.map((element: any) => {
        return this.runSaveFileService(element)
      }))

    } catch (e) {
      console.error('error', e)
      process.exit(1)
    } finally {
      process.exit(0)
    }
  }

  private async runSaveFileService(payload: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const worker = new Worker('./dist/workers/save-file-worker.js', {
        workerData: {
          path: './worker.ts',
          payload
        }
      })
      worker.on('error', reject)
      worker.on('exit', (code) => {
        if (code !== 0) reject(new Error(`Worker stopped with exit code ${code}`))
      })
    })
  }

}
