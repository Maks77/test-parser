import * as https from "https";

export class RequestManager {

  public static async getResponse(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      https.get(url, (res => {
        if (res.statusCode !== 200) {
          return reject(new Error('Bad response'))
        }
        res.setEncoding('utf8')

        const body: string[] = []

        res.on('data', (chunk) => {
          body.push(chunk)
        })

        res.on('end', () => {
          resolve(body.join(''))
        })

        res.on('error', (error) => reject(error))
      }))
    })
  }

}
