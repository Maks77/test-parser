export type WorkerDataType = {
  path: string
  payload: any
}
