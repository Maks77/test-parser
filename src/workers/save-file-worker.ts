import { workerData } from 'worker_threads'
import { WorkerDataType } from "../types";
import * as fs from 'fs'


const { payload }: WorkerDataType = workerData
const dir = './output'
const stringData = JSON.stringify(payload, null, '\t')


if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir)
}
fs.writeFile(`${dir}/${payload.data.id}.json`, stringData, (err) => {
  if (err) {
    console.error('writeFileError', err)
    return
  }
})
